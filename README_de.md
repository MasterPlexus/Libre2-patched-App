# Wie man die Librelink-App patcht, um xDrip per Bluetooth mit Werten zu versorgen, die direkt vom Sensor empfangen werden.


**WENN SIE DAS VERWENDEN MÖCHTEN, MÜSSEN SIE NACH ABSCHLUSS DER INSTALLATION EINEN NEUEN SENSOR AKTIVIEREN, DIE GEPACHTE APP FUNKTIONIERT NICHT MIT BEREITS AKTIVIERTEN SENSOREN!**.

**Bereits aktivierte Sensoren senden auch keine Alarme mehr an das Mobilteilgerät, wenn sie mit der nicht gepatchten Telefon-App aktiviert wurden.**


## Holen Sie sich die APK und die Tools.
1. Laden Sie die Version 2.3.0 der Freestyle Libre App DE, z.B. von[hier](https://apkpure.com/de/freestyle-librelink-de/com.freestylelibre.app.de) oder[hier](https://apkpure.com/de/freestyle-librelink-de/com.freestylelibre.app.de_com.freestylelibre.app.de_2019-04-22.apk/).
(Die deutsche Version hat eine englische Sprache, keine Sorge.)

2. [OPTIONAL] Wenn Sie überprüfen möchten, ob Ihre Version korrekt ist, überprüfen Sie den MD5 (für Windows benötigen Sie möglicherweise eine App wie: 
http://www.winmd5.com, um die md5-Prüfsumme zu erstellen). Die Checksumme sollte sein: `420735605bacf0e18d2570079ebaa238`

3. Laden Sie das [apkTool](https://ibotpeaches.github.io/Apktool/) und installieren Sie es wie beschrieben[hier](https://ibotpeaches.github.io/Apktool/install/). Es scheint, dass die aktuelle 2.4 Version von Apktool einige Probleme unter Windows hat, bitte laden Sie eine ältere 2.3 Version herunter und installieren Sie sie, wenn Sie Windows verwenden!

4. Öffnen Sie eine CMD-Eingabeaufforderung (Windows) oder Shell (\*nix), in der Sie die apk heruntergeladen haben (z.B. Ihren Download-Ordner).

5. Benutzen Sie das apktool: `apktool d -o librelink [FILENAME_OF_THE_DOWNLOADED_APK]`

6. Sie sollten nun einen Ordner namens `librelink` mit allen extrahierten Dateien der Anwendung haben.

7. Kopieren Sie die Patch-Dateien aus diesem Repository an den gleichen Ort wie der Ordner `librelink` (Sie sollten nun ein Verzeichnis mit mindestens den folgenden Dateien/Ordnern haben: `librelink`, `xdrip2.git.patch`, `sources` und `xdrip2.patch`).

## Patch-Anwendung 
**Verwenden Sie eine der beiden Methoden:**

### OPTION 1: Patch-Methode
1. Für Windows erhalten Sie das Patch-Tool von[hier](http://gnuwin32.sourceforge.net/packages/patch.htm) und installieren es. Mac, Linux und *nix sollten die App 'Patch' schon installiert haben, wenn nicht von Ihrem vertrauenswürdigen Repository aus installiert werden (z.B. sudo apt-get install patch).
2. Wechseln Sie in der Eingabeaufforderung/Shell in den Ordner `librelink`.
3. Verwenden Sie das Patch-Tool mit der Patch-Datei: `patch -p1 --binary --merge < ../xdrip2.patch`. für Windows `patch -p1 --binary --merge < ..\xdrip2.patch`. Stellen Sie sicher, dass Sie CMD-Prompt und nicht PowerShell verwenden, da PowerShell die '<' funktion nicht unterstützt.

### OPTION 2: git apply methode
1. Machen Sie Ihren Git-Client bereit. Für Windows installieren Sie so etwas wie[dies](https://gitforwindows.org)
2. Wechseln Sie in der Eingabeaufforderung/Shell in den Ordner `librelink`.
3. Verwenden Sie git, um den Patch anzuwenden: `git apply ../xdrip2.git.patch`, für Windows `git apply ..\xdrip2.git.patch`.

## Dateien hinzufügen
1. Navigieren Sie in Ihrem Dateibrowser innerhalb des librelink-Ordners zum Pfad `smali_classes2/com/librelink/app/`.
2. Kopieren Sie die folgenden Dateien aus dem Ordner `sources` dieses Repositorys in diesen Ordner:
	1. Drittanbieter-Integration.smali".
	2. `ForegroundService.smali`
	3. `CrashReportUtil.smali`.
	4. `APKExtractor.smali`
3. Benennen Sie das heruntergeladene APK in `original.apk` um.
4. Kopieren Sie die `original.apk` in den Ordner `assets` im Ordner `librelink`.

## Das APK wieder aufbauen

1. Gehen Sie in der Eingabeaufforderung/Shell in den Ordner, der den Ordner `librelink` enthält.
2. Verwenden Sie apktool, um das APK neu zu erstellen: `apktool b -o librelink.apk librelink`
3. Generieren Sie einen Signing-Keystore: 'keytool -genkey -v -keystore librelink.keystore -alias librelink -keyalg RSA -keysize 2048 -validity 10000`
	1. Füllen Sie die notwendigen Informationen aus. Du musst zwei Passwörter eingeben, eines für den Schlüsselspeicher, eines für den Schlüssel, du solltest sie dir notieren! Ich ziehe es vor, eher einfache Passwörter wie beide Male `librelink` zu verwenden, da dieser Schlüsselspeicher nur für die neu signierte App ist.
	2. **Sollte dies fehlschlagen, installieren Sie bitte aktuelle Java-Entwicklertools wie[this](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)**
	3. Wenn Sie die App später aktualisieren müssen, lassen Sie diesen Schritt weg, da der Keystore nun verfügbar ist.
4. Signieren Sie die APK: `jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore librelink.keystore librelink.keystore librelink.apk librelink`. Wenn der Jarsigner unter Windows nicht gefunden wird, überprüfen Sie bitte, ob Ihre Java-Variablen korrekt eingerichtet sind, wie beschrieben[hier](https://stackoverflow.com/questions/1672281/environment-variables-for-java-installation).

## Installieren Sie die neue APK.
5. Deinstallieren Sie die aktuelle librelink app auf dem Telefon.
6. Kopieren Sie die generierte `librelink.apk` auf Ihr Handy (USB, Google-Laufwerk, .... **gmail funktioniert nicht**) und installieren Sie sie.

## Abgeschlossen
**Öffnen Sie die App, gehen Sie zu "Alarme" und geben Sie alle notwendigen Berechtigungen, die die App anfordert, oder der erste Sensor, den Sie mit der App starten, sendet keine Daten über Bluetooth!** Alternativ können Sie in Ihren Android-Einstellungen -> Sicherheit zu der App navigieren und dort alle Berechtigungen (Ort und Dateien) erlauben.

Wenn alle Schritte erfolgreich ausgeführt wurden, haben Sie jetzt eine gepatchte librelink App, die in der Lage ist, ihre Daten an[xDrip] zu senden (https://github.com/jamorham/xDrip-plus).